﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="CCSymbols" Type="Str">IMAQ_USB,no;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">false</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="external.lib" Type="Folder">
			<Item Name="HGF_MenueTools.lvlib" Type="Library" URL="../../HGF_MenueTools/HGF_MenueTools.lvlib"/>
			<Item Name="LVDimInterface.lvlib" Type="Library" URL="../../../../LVSCC/LV2011/GPL/lib.lib/DimLVEvent/LVDimInterface/LVDimInterface.lvlib"/>
			<Item Name="DimIndicators.lvlib" Type="Library" URL="../../../../LVSCC/LV2011/GPL/lib.lib/DimLVEvent/DimIndicators/DimIndicators.lvlib"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="NI Instrument Simulator.lvlibp" Type="LVLibp" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp">
				<Item Name="Public" Type="Folder">
					<Item Name="Configure" Type="Folder">
						<Item Name="Configure Waveform.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure Waveform.vi"/>
						<Item Name="Configure Waveform Format.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure Waveform Format.vi"/>
						<Item Name="Configure DC Voltage.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure DC Voltage.vi"/>
						<Item Name="Configure.mnu" Type="Document" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Configure/Configure.mnu"/>
					</Item>
					<Item Name="Data" Type="Folder">
						<Item Name="Read Waveform.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Data/Read Waveform.vi"/>
						<Item Name="Read DC Measurement.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Data/Read DC Measurement.vi"/>
						<Item Name="Data.mnu" Type="Document" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Data/Data.mnu"/>
					</Item>
					<Item Name="Utility" Type="Folder">
						<Item Name="Reset.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Reset.vi"/>
						<Item Name="Revision Query.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Revision Query.vi"/>
						<Item Name="Self-Test.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Self-Test.vi"/>
						<Item Name="Write.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Write.vi"/>
						<Item Name="Read.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Read.vi"/>
						<Item Name="Utility.mnu" Type="Document" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Utility/Utility.mnu"/>
					</Item>
					<Item Name="dir.mnu" Type="Document" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/dir.mnu"/>
					<Item Name="Close.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Close.vi"/>
					<Item Name="Initialize.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/Initialize.vi"/>
					<Item Name="VI Tree.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Public/VI Tree.vi"/>
				</Item>
				<Item Name="Private" Type="Folder">
					<Item Name="Get Waveform Scaling.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/instr.lib/NI Instrument Simulator/Private/Get Waveform Scaling.vi"/>
				</Item>
				<Item Name="NI Instrument Simulator Readme.html" Type="Document" URL="../../../../LVPPL/LV2011/NI Instrument Simulator Readme.html"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../LVPPL/LV2011/NI Instrument Simulator.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
		</Item>
		<Item Name="HGF_BaseClasses" Type="Folder">
			<Item Name="HGF_VisionTools.lvlib" Type="Library" URL="../../HGF_LVC/HGF_VisionTools/HGF_VisionTools.lvlib"/>
			<Item Name="HGF_Auxiliary.lvlib" Type="Library" URL="../../HGF_LVC/HGF_Auxiliary/HGF_Auxiliary.lvlib"/>
		</Item>
		<Item Name="Examples" Type="Folder">
			<Item Name="HB_BaseClass_Examples.lvlib" Type="Library" URL="../HB_BaseClass_Examples.lvlib"/>
			<Item Name="HB_Device_Examples.lvlib" Type="Library" URL="../HB_Device_Examples.lvlib"/>
			<Item Name="HB_DIM_Examples.lvlib" Type="Library" URL="../HB_DIM_Examples.lvlib"/>
			<Item Name="HB_IMAQ_Examples.lvlib" Type="Library" URL="../HB_IMAQ_Examples.lvlib"/>
			<Item Name="HB_Inherance_Example.lvlib" Type="Library" URL="../Inheritance/HB_Inherance_Example.lvlib"/>
			<Item Name="HGF_DIM_DFSM_Examples.lvlib" Type="Library" URL="../HGF_DIM_DFSM_Examples.lvlib"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Get Rendezvous Status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Get Rendezvous Status.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="Resize Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Resize Rendezvous.vi"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="NI_SC_LVSCCommonFiles.lvlib" Type="Library" URL="/&lt;vilib&gt;/Statechart/Common/NI_SC_LVSCCommonFiles.lvlib"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/IMAQdx.ctl"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="IMAQ GetFileInfo" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ GetFileInfo"/>
				<Item Name="Simple Grid Descriptor" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Simple Grid Descriptor"/>
				<Item Name="Image Unit" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Unit"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="IMAQ AVI Create" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/IMAQ AVI Create"/>
				<Item Name="AviRefnum.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/AviRefnum.ctl"/>
				<Item Name="IMAQ AVI Close" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/IMAQ AVI Close"/>
				<Item Name="IMAQ Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Dispose"/>
				<Item Name="IMAQ ReadFile" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ ReadFile"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="IMAQ AVI Write Frame" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/IMAQ AVI Write Frame"/>
				<Item Name="IMAQ Init.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Init.vi"/>
				<Item Name="imgInterfaceOpen.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgInterfaceOpen.vi"/>
				<Item Name="imgUpdateErrorCluster.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgUpdateErrorCluster.vi"/>
				<Item Name="imgSessionOpen.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionOpen.vi"/>
				<Item Name="IMAQRegisterSession.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/IMAQRegisterSession.vi"/>
				<Item Name="imgClose.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgClose.vi"/>
				<Item Name="IMAQ Close.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Close.vi"/>
				<Item Name="IMAQUnregisterSession.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/IMAQUnregisterSession.vi"/>
				<Item Name="IMAQ Snap.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Snap.vi"/>
				<Item Name="imgIsNewStyleInterface.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgIsNewStyleInterface.vi"/>
				<Item Name="SessionLookUp.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/SessionLookUp.vi"/>
				<Item Name="IMAQ StillColor Setup.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqcolor.llb/IMAQ StillColor Setup.vi"/>
				<Item Name="IMAQ StillColor Attribute.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqcolor.llb/IMAQ StillColor Attribute.vi"/>
				<Item Name="IMAQ Sequence.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Sequence.vi"/>
				<Item Name="imgPushScalingAndROI.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgPushScalingAndROI.vi"/>
				<Item Name="imgSetGetRoiInternal.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSetGetRoiInternal.vi"/>
				<Item Name="imgSetChannel.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSetChannel.vi"/>
				<Item Name="imgSetGetScaling.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSetGetScaling.vi"/>
				<Item Name="imgSetRoi.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSetRoi.vi"/>
				<Item Name="IMAQ Configure List.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Configure List.vi"/>
				<Item Name="imgCreateBufList.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgCreateBufList.vi"/>
				<Item Name="imgSessionAttribute.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionAttribute.vi"/>
				<Item Name="imgBufferElement.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgBufferElement.vi"/>
				<Item Name="imgDisposeBufList.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgDisposeBufList.vi"/>
				<Item Name="IMAQ Start.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Start.vi"/>
				<Item Name="imgEnsureEqualBorders.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgEnsureEqualBorders.vi"/>
				<Item Name="imgGetBufList.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgGetBufList.vi"/>
				<Item Name="imgMemLock.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgMemLock.vi"/>
				<Item Name="imgSessionConfigure.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionConfigure.vi"/>
				<Item Name="imgSessionAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionAcquire.vi"/>
				<Item Name="IMAQ Wait Signal.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqsignalio.llb/IMAQ Wait Signal.vi"/>
				<Item Name="imgWaitForIMAQOccurrence.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgWaitForIMAQOccurrence.vi"/>
				<Item Name="IMAQ Stop.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Stop.vi"/>
				<Item Name="imgSessionStopAcquisition.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionStopAcquisition.vi"/>
				<Item Name="imgPopScalingAndROI.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgPopScalingAndROI.vi"/>
				<Item Name="IMAQ Configure Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Configure Buffer.vi"/>
				<Item Name="imgSnap.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSnap.vi"/>
				<Item Name="IMAQ RectToCoord.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ RectToCoord.vi"/>
				<Item Name="IMAQ Attribute.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Attribute.vi"/>
				<Item Name="IMAQ SetImageSize" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ SetImageSize"/>
				<Item Name="IMAQ GetImagePixelPtr" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ GetImagePixelPtr"/>
				<Item Name="IMAQ Grab Setup.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Grab Setup.vi"/>
				<Item Name="imgGrabSetup.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgGrabSetup.vi"/>
				<Item Name="imgAssociateBufListWithInterface.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgAssociateBufListWithInterface.vi"/>
				<Item Name="imgPopScalingAndROIWhenFinished.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgPopScalingAndROIWhenFinished.vi"/>
				<Item Name="IMAQ Grab Acquire.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqhl.llb/IMAQ Grab Acquire.vi"/>
				<Item Name="IMAQ Grab Acquire Old Style.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/IMAQ Grab Acquire Old Style.vi"/>
				<Item Name="imgGrabArea.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgGrabArea.vi"/>
				<Item Name="IMAQ GetImageSize" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ GetImageSize"/>
				<Item Name="imgReconstructimage.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgReconstructimage.vi"/>
				<Item Name="imgGetBitsPerComponent.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgGetBitsPerComponent.vi"/>
				<Item Name="IMAQ Image Datatype to Image Cluster.vi" Type="VI" URL="/&lt;vilib&gt;/vision/DatatypeConversion.llb/IMAQ Image Datatype to Image Cluster.vi"/>
				<Item Name="IMAQ Image Cluster to Image Datatype.vi" Type="VI" URL="/&lt;vilib&gt;/vision/DatatypeConversion.llb/IMAQ Image Cluster to Image Datatype.vi"/>
				<Item Name="IMAQ Image Bit Depth" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Image Bit Depth"/>
				<Item Name="IMAQ Fit ROI.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Fit ROI.vi"/>
				<Item Name="imgSessionFitROI.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionFitROI.vi"/>
				<Item Name="IMAQ Set User LUT.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Set User LUT.vi"/>
				<Item Name="imgSessionSetUserLUT8bit.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionSetUserLUT8bit.vi"/>
				<Item Name="imgSessionSetUserLUT16bit.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionSetUserLUT16bit.vi"/>
				<Item Name="IMAQ Configure Trigger3.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqsignalio.llb/IMAQ Configure Trigger3.vi"/>
				<Item Name="imgSessionTriggerConfigure3.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionTriggerConfigure3.vi"/>
				<Item Name="imgSessionLineTrigSource2.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionLineTrigSource2.vi"/>
				<Item Name="IMAQ Get Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Get Buffer.vi"/>
				<Item Name="imgSessionWaitBuffer.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionWaitBuffer.vi"/>
				<Item Name="IMAQ Status.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Status.vi"/>
				<Item Name="imgSessionStatus.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgSessionStatus.vi"/>
				<Item Name="imgGetImageFromBufList.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgGetImageFromBufList.vi"/>
				<Item Name="IMAQ Copy Acquired Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/imaqll.llb/IMAQ Copy Acquired Buffer.vi"/>
				<Item Name="IMAQ Copy Acquired Buffer Old Style.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/IMAQ Copy Acquired Buffer Old Style.vi"/>
				<Item Name="imgCopyAreaWithOccur.vi" Type="VI" URL="/&lt;vilib&gt;/vision/driver/DLLCalls.llb/imgCopyAreaWithOccur.vi"/>
			</Item>
			<Item Name="HGF_ObjectManager.lvlib" Type="Library" URL="../../HGF_LVC/HGF_ObjectManager/HGF_ObjectManager.lvlib"/>
			<Item Name="HGF_FGV.lvlib" Type="Library" URL="../../HGF_LVC/HGF_BaseClasses/HGF_FGV/HGF_FGV.lvlib"/>
			<Item Name="HGF_DIM.lvlib" Type="Library" URL="../../HGF_LVC/HGF_DIM/HGF_DIM.lvlib"/>
			<Item Name="HGF_IMAQ.lvlib" Type="Library" URL="../../HGF_LVC/HGF_IMAQ/HGF_IMAQ.lvlib"/>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="imaq.dll" Type="Document" URL="imaq.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="HGF_BaseClasses.lvlib" Type="Library" URL="../../HGF_LVC/HGF_BaseClasses/HGF_BaseClasses.lvlib"/>
			<Item Name="HGF_DeviceBaseClasses.lvlib" Type="Library" URL="../../HGF_LVC/HGF_DeviceBaseClasses/HGF_DeviceBaseClasses.lvlib"/>
			<Item Name="HGF_DeviceFSM.lvlib" Type="Library" URL="../../HGF_LVC/HGF_StateCharts/HGF_DeviceFSM.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="HB_BaseClass_Examples" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HB_BaseClass_Examples</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes and demo VIs illustrating the application of HGF_LVC library and corresponding design patterns.</Property>
				<Property Name="DestinationID[0]" Type="Str">{BBFEB381-D559-4B9D-81FB-30FE9DA0A600}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">0</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2011/builds/HGF_LVC/HB_BaseClass_Examples.zip</Property>
				<Property Name="ZipBase" Type="Str">LVSCC\LV2009\GPL\User\Brand\HGF_DesignPatterns</Property>
			</Item>
			<Item Name="HB_DIM_Examples" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HB_DIM_Examples</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes and demo VIs illustrating the application of HGF_LVC library using DIM as network communication layer.</Property>
				<Property Name="DestinationID[0]" Type="Str">{BBFEB381-D559-4B9D-81FB-30FE9DA0A600}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">0</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2011/builds/HGF_LVC/HB_DIM_Examples.zip</Property>
				<Property Name="ZipBase" Type="Str">LVSCC\LV2009\GPL\User\Brand\HGF_DesignPatterns</Property>
			</Item>
			<Item Name="HB_Device_Examples" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HB_Device_Examples</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes and demo VIs illustrating the application of HGF_LVC library using the NI Instrument Simulator.</Property>
				<Property Name="DestinationID[0]" Type="Str">{BBFEB381-D559-4B9D-81FB-30FE9DA0A600}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">0</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2011/builds/HGF_LVC/HB_Device_Exampless.zip</Property>
				<Property Name="ZipBase" Type="Str">LVSCC\LV2009\GPL\User\Brand\HGF_DesignPatterns</Property>
			</Item>
			<Item Name="HB_DIM_DFSM_Device_Examples" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HB_DIM_DFSM_Device_Examples</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes and demo VIs illustrating the application of HGF_LVC library using the NI Instrument Simulator and DIM.</Property>
				<Property Name="DestinationID[0]" Type="Str">{BBFEB381-D559-4B9D-81FB-30FE9DA0A600}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">0</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2011/builds/HGF_LVC/HB_DIM_DFSM_Device_Exampless.zip</Property>
				<Property Name="ZipBase" Type="Str">LVSCC\LV2009\GPL\User\Brand\HGF_DesignPatterns</Property>
			</Item>
			<Item Name="HB_PollingAgent" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HB_PollingAgent</Property>
				<Property Name="Comments" Type="Str">This zip-file contains an exampüle for a Mobile Agent class.</Property>
				<Property Name="DestinationID[0]" Type="Str">{F2538F77-E6E3-437B-AFE6-AD2674E034F5}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">0</Property>
				<Property Name="IncludedItems[0]" Type="Ref">/My Computer/Dependencies/Items in Memory/HB_PollingAgent.lvlib</Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2011/builds/HGF_LVC/HB_PollingAgent.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HB_PollingAgentStarter" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{DE89CCE5-9161-4C0F-9B4F-3B6F64A5CB3D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{74F9BDB7-8CEC-4006-9CCE-7B8794856F16}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{23DF74C4-3E61-4839-AA85-0F76ABBE91A5}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HB_PollingAgentStarter</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/F/tmp/LV2011/builds/HGF_LVC/HB_PollingAgentStarter</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{50B21AA0-285A-46E5-9563-D6B002A06F06}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">HB_PollingAgentStarter.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/F/tmp/LV2011/builds/HGF_LVC/HB_PollingAgentStarter/HB_PollingAgentStarter.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/F/tmp/LV2011/builds/HGF_LVC/HB_PollingAgentStarter/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{EC4BC164-F951-46CF-893C-B62A0FE597A9}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum Für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HB_PollingAgentStarter</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HB_PollingAgentStarter</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2010 GSI Helmholtzzentrum Für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">HB_PollingAgentStarter</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{4BDDBEEF-E017-45BC-A7DF-04621C75A5E7}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">HB_PollingAgentStarter.exe</Property>
			</Item>
			<Item Name="HB_IMAQ_Examples" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HB_IMAQ_Examples</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes and demo VIs illustrating the application of HGF_LVC library and HGF_IMAQ classes.</Property>
				<Property Name="DestinationID[0]" Type="Str">{BBFEB381-D559-4B9D-81FB-30FE9DA0A600}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">0</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2011/builds/HGF_LVC/HB_IMAQ_Exampless.zip</Property>
				<Property Name="ZipBase" Type="Str">LVSCC\LV2009\GPL\User\Brand\HGF_DesignPatterns</Property>
			</Item>
		</Item>
	</Item>
</Project>
