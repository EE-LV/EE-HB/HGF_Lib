﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">false</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GPG" Type="Folder"/>
		<Item Name="SV" Type="Folder"/>
		<Item Name="HGF Classes" Type="Folder"/>
		<Item Name="HGF_MoAgSy" Type="Folder">
			<Item Name="Host.ico" Type="Document" URL="../Host.ico"/>
			<Item Name="MoAgSy.lvlib" Type="Library" URL="../MoAgSy.lvlib"/>
			<Item Name="MoAgSy_Base.lvlib" Type="Library" URL="../MoAgSy_Base.lvlib"/>
		</Item>
		<Item Name="license.txt" Type="Document" URL="../../license.txt"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Get Rendezvous Status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Get Rendezvous Status.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="Resize Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Resize Rendezvous.vi"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="PRC_DeleteVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteVar.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="DataSocket Select URL.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/dataskt.llb/DataSocket Select URL.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="NI_SC_LVSCCommonFiles.lvlib" Type="Library" URL="/&lt;vilib&gt;/Statechart/Common/NI_SC_LVSCCommonFiles.lvlib"/>
				<Item Name="SCRT SDV Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT SDV Rtn.vi"/>
				<Item Name="SCRT Dbg Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT Dbg Rtn.vi"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_InternetTK_FTP_VIs.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/internet/NI_InternetTK_FTP_VIs.lvlib"/>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/E/Program Files/National Instruments/LabVIEW 2010/resource/logosbrw.dll"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="HGF_BaseClasses.lvlib" Type="Library" URL="../../HGF_BaseClasses/HGF_BaseClasses.lvlib"/>
			<Item Name="HGF_Auxiliary.lvlib" Type="Library" URL="../../HGF_Auxiliary/HGF_Auxiliary.lvlib"/>
			<Item Name="HGF_GPG.lvlib" Type="Library" URL="../../HGF_GPG/HGF_GPG.lvlib"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/E/Program Files/National Instruments/LabVIEW 2010/resource/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/E/Program Files/National Instruments/LabVIEW 2010/resource/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/E/Program Files/National Instruments/LabVIEW 2010/resource/dialog/variable/SCT Get Types.vi"/>
			<Item Name="HGF_SV.lvlib" Type="Library" URL="../../HGF_SV/HGF_SV.lvlib"/>
			<Item Name="HGF_DSC.lvlib" Type="Library" URL="../../HGF_DSC/HGF_DSC.lvlib"/>
			<Item Name="DimLVInterface.lvlibp" Type="LVLibp" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp">
				<Item Name="typedefs" Type="Folder">
					<Item Name="LVDimInterface.dim service type.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim service type.ctl"/>
					<Item Name="LVDimInterface.notifier type.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.notifier type.ctl"/>
					<Item Name="LVDimInterface.queue type.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.queue type.ctl"/>
				</Item>
				<Item Name="public" Type="Folder">
					<Item Name="client" Type="Folder">
						<Item Name="LVDimInterface.dic_info_service.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_info_service.vi"/>
						<Item Name="LVDimInterface.dic_info_service_notifier.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_info_service_notifier.vi"/>
						<Item Name="LVDimInterface.dic_info_service_queue.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_info_service_queue.vi"/>
						<Item Name="LVDimInterface.dic_release_service.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_release_service.vi"/>
						<Item Name="LVDimInterface.dic_info_service_once.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_info_service_once.vi"/>
						<Item Name="LVDimInterface.dic_command_service.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_command_service.vi"/>
						<Item Name="LVDimInterface.dic_command_service_local.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dic_command_service_local.vi"/>
					</Item>
					<Item Name="server" Type="Folder">
						<Item Name="LVDimInterface.dis_add_command.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_add_command.vi"/>
						<Item Name="LVDimInterface.dis_remove_command.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_remove_command.vi"/>
						<Item Name="LVDimInterface.dis_remove_service.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_remove_service.vi"/>
						<Item Name="LVDimInterface.dis_set_quality.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_set_quality.vi"/>
						<Item Name="LVDimInterface.dis_set_time_stamp.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_set_time_stamp.vi"/>
						<Item Name="LVDimInterface.dis_start_serving.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_start_serving.vi"/>
						<Item Name="LVDimInterface.dis_stop_serving.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_stop_serving.vi"/>
						<Item Name="LVDimInterface.dis_update_service.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_update_service.vi"/>
						<Item Name="LVDimInterface.dis_add_command_notifier.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_add_command_notifier.vi"/>
						<Item Name="LVDimInterface.dis_add_command_queue.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_add_command_queue.vi"/>
						<Item Name="LVDimInterface.dis_add_service.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dis_add_service.vi"/>
					</Item>
					<Item Name="common" Type="Folder">
						<Item Name="LVDimInterface.dim_disable_security.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_disable_security.vi"/>
						<Item Name="LVDimInterface.dim_enable_security.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_enable_security.vi"/>
						<Item Name="LVDimInterface.dim_get_info.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_get_info.vi"/>
						<Item Name="LVDimInterface.dim_get_service_info.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_get_service_info.vi"/>
						<Item Name="LVDimInterface.dim_operate.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_operate.vi"/>
						<Item Name="LVDimInterface.dim_unfold_data.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_unfold_data.vi"/>
						<Item Name="LVDimInterface.dim_unfold_data_info.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_unfold_data_info.vi"/>
						<Item Name="LVDimInterface.error_message.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.error_message.vi"/>
						<Item Name="LVDimInterface.get library version.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.get library version.vi"/>
					</Item>
					<Item Name="examples" Type="Folder">
						<Item Name="LVDimInterface.application example client.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.application example client.vi"/>
						<Item Name="LVDimInterface.application example serv buffer.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.application example serv buffer.vi"/>
						<Item Name="LVDimInterface.application example serv command.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.application example serv command.vi"/>
						<Item Name="LVDimInterface.application example serv command_double_array.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.application example serv command_double_array.vi"/>
						<Item Name="LVDimInterface.application example serv command_integer_double.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.application example serv command_integer_double.vi"/>
					</Item>
					<Item Name="TestTools" Type="Folder">
						<Item Name="LVDimInterface.test ping pong client.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/TestTools/LVDimInterface.test ping pong client.vi"/>
						<Item Name="LVDimInterface.test ping pong server.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/TestTools/LVDimInterface.test ping pong server.vi"/>
						<Item Name="LVDimInterface.test simple client.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/TestTools/LVDimInterface.test simple client.vi"/>
						<Item Name="LVDimInterface.test simple server.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/TestTools/LVDimInterface.test simple server.vi"/>
					</Item>
				</Item>
				<Item Name="private" Type="Folder">
					<Item Name="LVDimInterface.dim_dispatcher.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_dispatcher.vi"/>
					<Item Name="LVDimInterface.dim_fire_occurrence.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_fire_occurrence.vi"/>
					<Item Name="LVDimInterface.dim_get_occurrence_ref.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_get_occurrence_ref.vi"/>
					<Item Name="LVDimInterface.dim_set_occurrence_ref.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_set_occurrence_ref.vi"/>
					<Item Name="LVDimInterface.dim_semaphore.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_semaphore.vi"/>
					<Item Name="LVDimInterface.dim_set_DNS_node.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.dim_set_DNS_node.vi"/>
				</Item>
				<Item Name="forFriendsOnly" Type="Folder">
					<Item Name="LVDimInterface.release.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.release.vi"/>
					<Item Name="LVDimInterface.acquire.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.acquire.vi"/>
				</Item>
				<Item Name="supportFiles" Type="Folder">
					<Item Name="performance_DIM_LV2009.xls" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/performance_DIM_LV2009.xls"/>
					<Item Name="libDimWrapper.dll" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/libDimWrapper.dll"/>
					<Item Name="libDimWrapperSPL.dll" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/libDimWrapperSPL.dll"/>
					<Item Name="msvcr70.dll" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/msvcr70.dll"/>
					<Item Name="msvcr70d.dll" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/msvcr70d.dll"/>
					<Item Name="myDimSPL.dll" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/myDimSPL.dll"/>
					<Item Name="myDimStd.dll" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/myDimStd.dll"/>
					<Item Name="libmydim.a" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/libmydim.a"/>
					<Item Name="libmydim.so" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/libmydim.so"/>
					<Item Name="libDimWrapper.so" Type="Document" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/libDimWrapper.so"/>
				</Item>
				<Item Name="LVDimInterface.VI-Tree.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/LVDimInterface/LVDimInterface.VI-Tree.vi"/>
				<Item Name="DimIndicators.lvlib" Type="Library" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/DimIndicators/DimIndicators.lvlib"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="DialogType.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="../../../../../GIT-SVN/GPL/lib.lib/DimLVEvent/PPL/DimLVInterface.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Release Semaphore.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="MoAgSy_Host" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{8439978D-92C4-48F5-95B7-1C42A1263139}</Property>
				<Property Name="App_INI_GUID" Type="Str">{54CABECC-F9F4-4030-AA60-A98895445F3C}</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">This executable runs a Host for the LVOOP Mobile Agent System.</Property>
				<Property Name="Bld_buildSpecName" Type="Str">MoAgSy_Host</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/F/tmp/LV2009/builds/HGF_LVC/MoAgSy/Application</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_targetDestDir" Type="Path"></Property>
				<Property Name="Destination[0].destName" Type="Str">MoAgSy_Host.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/F/tmp/LV2009/builds/HGF_LVC/MoAgSy/Application/MoAgSy_Host.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/F/tmp/LV2009/builds/HGF_LVC/MoAgSy/Application/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">DIM</Property>
				<Property Name="Destination[2].path" Type="Path">/F/tmp/LV2009/builds/HGF_LVC/MoAgSy/Application/DIM</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">GPG</Property>
				<Property Name="Destination[3].path" Type="Path">/F/tmp/LV2009/builds/HGF_LVC/MoAgSy/Application/GPG</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="Exe_iconItemID" Type="Ref"></Property>
				<Property Name="Source[0].itemID" Type="Str">{7B17A6B9-8F69-40C8-AB6A-61EFA0CB57F0}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref"></Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref"></Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">8</Property>
				<Property Name="TgtF_autoIncrement" Type="Bool">true</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">MoAgSy_Host

Contact: Freddy.Berck@gmx.de or H.Brand@gsi.de</Property>
				<Property Name="TgtF_fileVersion.build" Type="Int">2</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">MoAgSy_Host</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2010 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">MoAgSy_Host</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{8BF9F659-2F8D-42FB-B53B-C85AE2CCE05D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">MoAgSy_Host.exe</Property>
			</Item>
			<Item Name="Mobile Agent System" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">HGF_MobileAgent</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">data</Property>
				<Property Name="Destination[1].parent" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Destination[1].tag" Type="Str">{51F61494-6C25-452E-8E21-24138D0B3319}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="Destination[2].name" Type="Str">DIM</Property>
				<Property Name="Destination[2].parent" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Destination[2].tag" Type="Str">{5847D579-C743-44F1-95F3-D27D450B6CE5}</Property>
				<Property Name="Destination[2].type" Type="Str">userFolder</Property>
				<Property Name="Destination[3].name" Type="Str">GPG</Property>
				<Property Name="Destination[3].parent" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Destination[3].tag" Type="Str">{CE76EBFF-3642-48E8-B59C-456ECB14D64A}</Property>
				<Property Name="Destination[3].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{4C1F4C94-7160-4A79-8BE3-4BACCFDE93DF}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2010 SP1</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{9F6EADB1-707C-41AF-8F3D-FB856FA8BD1C}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{C1D5338B-5582-4CC2-9AB9-23DBF97D97C5}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Enhanced DSC Deployment Support for LabVIEW 2010</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{AD4C8E45-871C-47C7-8471-D4AB0A169712}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{39196150-E2F8-4502-82E3-B54C1F8E0920}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI Distributed System Manager 2009 SP1</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{38953690-FC7E-4766-9045-A8C5DF9AEDE9}</Property>
				<Property Name="DistPartCount" Type="Int">3</Property>
				<Property Name="INST_author" Type="Str">GSI Darmstadt</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/F/tmp/LV2009/builds/HGF_LVC/MoAgSy/Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">Mobile Agent System</Property>
				<Property Name="INST_defaultDir" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="INST_productName" Type="Str">HGF Mobile Agent System</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.3</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">10018002</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">H.Brand@gsi.de</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.gsi.de/</Property>
				<Property Name="MSI_distID" Type="Str">{58851E2B-2454-4CA7-9C3C-E2DC2B2F369F}</Property>
				<Property Name="MSI_licenseID" Type="Ref"></Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{930BC752-3030-46BB-97E0-06E89DCEE188}</Property>
				<Property Name="MSI_windowMessage" Type="Str">This is a prototype of the  HGF Mobile Agent System.</Property>
				<Property Name="MSI_windowTitle" Type="Str">Installer for HGF Mobile Agent System</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Source[0].File[0].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">MoAgSy_Host.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">MoAgSy_Host</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">HGF Mobile Agent System</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{8BF9F659-2F8D-42FB-B53B-C85AE2CCE05D}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">2</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{89D724FB-A976-49BD-A646-271637C4C2B6}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">MoAgSy_Host.aliases</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{8439978D-92C4-48F5-95B7-1C42A1263139}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">MoAgSy_Host</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/MoAgSy_Host</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="HGF_MobileAgent" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_MobileAgent</Property>
				<Property Name="Comments" Type="Str">This zip-file contains the HGF MoAgSy_Base classes.</Property>
				<Property Name="DestinationID[0]" Type="Str">{6683335C-D84B-4558-AFD8-B9E02C4CE851}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">4</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludedItems[2]" Type="Ref"></Property>
				<Property Name="IncludedItems[3]" Type="Ref">/My Computer/license.txt</Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_MobileAgent.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
		</Item>
	</Item>
</Project>
