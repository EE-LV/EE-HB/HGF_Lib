﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This project contains all HGF class libraries and some HGF auxiliary VIs.

HGF means: Helmholz, GSI &amp; FAIR

Author: H.Brand@gsi.de

History:
Revision 1.0.0.0: Aug 26, 2010 H.Brand@gsi.de; First release.

Copyright 2010 GSI

GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstr. 1, 64291 Darmstadt, Germany
Contact: H.Brand@gsi.de or D.Beck@gsi.de

This file is part of the HGF LVOOP class library.

    HGF LVOOP class library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HGF LVOOP class library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HGF LVOOP class library.  If not, see http://www.gnu.org/licenses/.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">false</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="HGF_BaseClasses" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_BaseClasses</Property>
				<Property Name="Comments" Type="Str">This zip-file contains the HGF LVOOP base classes.</Property>
				<Property Name="DestinationID[0]" Type="Str">{49699A0A-1678-4394-95E0-E0B4AF5F289C}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">5</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludedItems[2]" Type="Ref"></Property>
				<Property Name="IncludedItems[3]" Type="Ref"></Property>
				<Property Name="IncludedItems[4]" Type="Ref"></Property>
				<Property Name="IncludedItems[5]" Type="Ref"></Property>
				<Property Name="IncludedItems[6]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../tmp/LV2009/builds/HGF_LVC/HGF_BaseClasses.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_SharedVariable" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_SharedVariable</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes to interface to Shared Variables.

The included classes depend on:
- HGF_BaseClasses.lvlib in HGF_BaseClasses.zip
</Property>
				<Property Name="DestinationID[0]" Type="Str">{691380DA-4B13-494F-A2DD-10C55A0BFD19}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">2</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_SharedVariable.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_DSC" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_DSC</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes to access Shared Variables via DSC Module (Data Logging and Supervisory Control).

The included classes depend on:
- HGF_BaseClasses.lvlib in HGF_BaseClasses.zip
- HGF_SV.lvlib included in HGF_SharedVariable.zip</Property>
				<Property Name="DestinationID[0]" Type="Str">{229D854C-2937-48FE-8114-0A12D98D7FE4}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">2</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_DSC.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_DIM" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_DIM</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes to interface to DIM.

The included classes depend on:
- HGF_BaseClasses.lvlib in HGF_BaseClasses.zip</Property>
				<Property Name="DestinationID[0]" Type="Str">{0DED8977-ECEF-45E1-B5B6-EA204FCA109B}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">2</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_DIM.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_IMAQ" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_IMAQ</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes to access cameras via NI-IMAQ or NI-IMAQdx.

The included classes depend on:
- HGF_BaseClasses.lvlib in HGF_BaseClasses.zip</Property>
				<Property Name="DestinationID[0]" Type="Str">{07676E54-D728-42A7-8AB3-868DC3A44398}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">3</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludedItems[2]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_IMAQ.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_DeviceFSM" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_DeviceFSM</Property>
				<Property Name="Comments" Type="Str">This zip-file contains classes to drive stationary device agents by using the StateChart Module.

The included classes depend on:
- HGF_BaseClasses.lvlib in HGF_BaseClasses.zip
</Property>
				<Property Name="DestinationID[0]" Type="Str">{4BBA07A3-9F4F-4493-B198-1C64BED21F20}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">2</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_DeviceFSMs.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_GPG" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">HGF_GPG</Property>
				<Property Name="Comments" Type="Str">This zip-file contains the HGF GPG base classes.</Property>
				<Property Name="DestinationID[0]" Type="Str">{49699A0A-1678-4394-95E0-E0B4AF5F289C}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">2</Property>
				<Property Name="IncludedItems[0]" Type="Ref"></Property>
				<Property Name="IncludedItems[1]" Type="Ref"></Property>
				<Property Name="IncludedItems[2]" Type="Ref"></Property>
				<Property Name="IncludedItems[3]" Type="Ref"></Property>
				<Property Name="IncludedItems[4]" Type="Ref"></Property>
				<Property Name="IncludedItems[5]" Type="Ref"></Property>
				<Property Name="IncludeProject" Type="Bool">false</Property>
				<Property Name="Path[0]" Type="Path">../../../../../../tmp/LV2009/builds/HGF_LVC/HGF_GPG.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
			<Item Name="HGF_Auxiliary.lvlibp" Type="Packed Library">
				<Property Name="Bld_buildSpecName" Type="Str">HGF_Auxiliary.lvlibp</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/F/LVDev/PPL</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">HGF_Auxiliary.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/F/LVDev/PPL/HGF_Auxiliary.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/F/LVDev/PPL</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{1354BF9E-D6DB-4552-B170-5D308571F444}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_autoIncrement" Type="Bool">true</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HGF_Auxiliary.lvlibp</Property>
				<Property Name="TgtF_fileVersion.build" Type="Int">1</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HGF_Auxiliary.lvlibp</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2011 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">HGF_Auxiliary.lvlibp</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{120D3CBF-7079-4BAD-81B9-9FE013A4B2CF}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">HGF_Auxiliary.lvlibp</Property>
			</Item>
			<Item Name="HGF_BaseClasses.lvlibp" Type="Packed Library">
				<Property Name="Bld_buildSpecName" Type="Str">HGF_BaseClasses.lvlibp</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/HGF_Classes/HGF_BaseClasses.lvlibp</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">HGF_BaseClasses.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/HGF_BaseClasses.lvlibp/HGF_BaseClasses.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/HGF_BaseClasses.lvlibp</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{F4256E43-F5E4-4CAC-A4BE-BE9386B1534A}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].properties[0].type" Type="Str">Remove front panel</Property>
				<Property Name="Source[2].properties[0].value" Type="Bool">false</Property>
				<Property Name="Source[2].properties[1].type" Type="Str">Remove block diagram</Property>
				<Property Name="Source[2].properties[1].value" Type="Bool">false</Property>
				<Property Name="Source[2].propertiesCount" Type="Int">2</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_autoIncrement" Type="Bool">true</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HGF_BaseClasses.lvlibp</Property>
				<Property Name="TgtF_fileVersion.build" Type="Int">2</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HGF_BaseClasses.lvlibp</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2011 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">HGF_BaseClasses.lvlibp</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{D77FA1DB-5BD9-4ABD-88C3-EE881A8B42EF}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">HGF_BaseClasses.lvlibp</Property>
			</Item>
		</Item>
	</Item>
</Project>
